export class Employee {
    id!: number;
    employee_name!: String;
    employee_salary!: number;
    employee_age!: number;
    employee_anual_salary!: number;
    profile_image!: string;
}

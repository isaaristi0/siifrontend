import { Component } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Employee } from '../models/employee.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL } from '../app.config';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {
  idFormControl = new FormControl('', [Validators.pattern('^[0-9]*$')]);

  matcher = new MyErrorStateMatcher();
  displayedColumns: string[] = ['id', 'employee_name', 'employee_age', 'employee_salary', 'profile_image', 'employee_anual_salary'];
  public respuestaList: Array<Employee> = [];
  public id: any;
  constructor(public http: HttpClient) { }

  buscar() {
    this.id = this.idFormControl.value;
    if (!this.idFormControl.value) {
      this.empleados();
    } else {
      this.empleadoId();
    }
  }
  empleados() {
    let contentType = new HttpHeaders();

    contentType.append('Content-Type', 'application/json');
    contentType.append('Accept', 'application/json');

    contentType.append('Access-Control-Allow-Origin', 'http://127.0.0.1:8081');
    contentType.append('Access-Control-Allow-Credentials', 'true');

    contentType.append('GET', 'POST');

    this.http.get(URL + "/employee/employeeList", { headers: contentType }).subscribe((response: any) => {
      this.respuestaList = JSON.parse(JSON.stringify(response))
    });
  }

  empleadoId() {
    let contentType = new HttpHeaders();
    contentType.append('Content-Type', 'application/json');
    contentType.append('Accept', 'application/json');

    contentType.append('Access-Control-Allow-Origin', 'http://127.0.0.1:8081');
    contentType.append('Access-Control-Allow-Credentials', 'true');

    contentType.append('GET', 'POST');

    this.http.get(URL + "/employee/" + parseInt(this.id), { headers: contentType }).subscribe((response: any) => {
      this.respuestaList = JSON.parse(JSON.stringify(response))
    });
  }

}
